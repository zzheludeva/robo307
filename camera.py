import cv2
import numpy as np

cap = cv2.VideoCapture(0)
if not cap.isOpened():
    print("Cannot open camera")
    exit()


def filterConter(box: np.array) -> bool:
    return cv2.contourArea(box) > 100.0

lastpink = []
lastblue = []

def drawNewCoutours(img, contours, color, type):
    global lastpink
    global lastblue

    boxMax = max(contours, key=cv2.contourArea)
    rect = cv2.minAreaRect(boxMax)  # пытаемся вписать прямоугольник
    box = cv2.boxPoints(rect)  # поиск четырех вершин прямоугольника
    box = np.int0(box) 
    
    if type == 'pink':
        lastCounter = lastpink.copy()
        lastpink = box
    else :
        lastCounter = lastblue.copy()
        lastblue = box

    len(lastCounter) != 0 and print(np.subtract(box, lastCounter))
    filterConter(box) and cv2.drawContours(img, [box], 0, color, 2)

    return img


# Create an image with a black background
while True:
    # Capture frame-by-frame
    ret, img = cap.read()
    # if frame is read correctly ret is True
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)


    # Convert the RGB values to HSV format
    rgb_pink = (241, 37, 135)  # color
    hsv_pink = cv2.cvtColor(np.uint8([[rgb_pink]]), cv2.COLOR_RGB2HSV)[0][0]
    rgb_blue = (34, 143, 244)  # color
    hsv_blue = cv2.cvtColor(np.uint8([[rgb_blue]]), cv2.COLOR_RGB2HSV)[0][0]

    # Define the range of values that represent the desired color
    hue_range = 5  # allowed variation in hue
    saturation_range = 50  # allowed variation in saturation
    value_range = 50  # allowed variation in value

    lower_color_pink = np.array([hsv_pink[0] - hue_range, hsv_pink[1] - saturation_range, hsv_pink[2] - value_range])
    upper_color_pink = np.array([hsv_pink[0] + hue_range, hsv_pink[1] + saturation_range, hsv_pink[2] + value_range])

    lower_color_blue = np.array([hsv_blue[0] - hue_range, hsv_blue[1] - saturation_range, hsv_blue[2] - value_range])
    upper_color_blue = np.array([hsv_blue[0] + hue_range, hsv_blue[1] + saturation_range, hsv_blue[2] + value_range])

    # Apply color thresholding to extract the color of interest
    mask_pink = cv2.inRange(hsv, lower_color_pink, upper_color_pink)
    mask_blue = cv2.inRange(hsv, lower_color_blue, upper_color_blue)

    contours_pink, hierarchy_pink = cv2.findContours(mask_pink, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours_blue, hierarchy_blue = cv2.findContours(mask_blue, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    img = drawNewCoutours(img, contours_blue, (255, 0, 0), 'blue') if len(contours_blue) !=0 else img
    img = drawNewCoutours(img, contours_pink, (0, 0, 255), 'pink') if len(contours_pink) !=0 else img
    
    cv2.imshow('Camera', img)
    if cv2.waitKey(1) == ord('q'):
        break
# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
# # Show the image
#     cv2.imshow('Circles', img)
#     cv2.waitKey(0)
#     cv2.destroyAllWindows()

